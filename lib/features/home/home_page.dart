import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/features/home/widget/pokemon_list.dart';
import 'package:pokemon_app/features/view_pokemon/pokemon_details_page.dart';
import 'package:pokemon_app/state/actions.dart';
import 'package:pokemon_app/state/app_state.dart';

class _HomePageVM extends BaseModel<AppState> {
  _HomePageVM();

  Pokemon pokemon;
  Function() onLoadMoreTapped;

  _HomePageVM.build({
    this.pokemon,
    this.onLoadMoreTapped,
  }) : super(equals: [pokemon]);

  @override
  BaseModel fromStore() {
    return _HomePageVM.build(
        pokemon: state.pokemon,
        onLoadMoreTapped: () => dispatchFuture(GetAllPokemonAction()));
  }
}

class HomePage extends StatelessWidget {
  static const route = 'home';

  void _navigateToPokemonDetailsPage(BuildContext context, Result result) {
    Navigator.pushNamed(context, PokemonDetailsPage.route,
        arguments: PokemonDetailsArguments(result.getId()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokemon'),
      ),
      body: StoreConnector<AppState, _HomePageVM>(
        onInit: (store) => store.dispatchFuture(GetAllPokemonAction()),
        model: _HomePageVM(),
        builder: (BuildContext context, _HomePageVM vm) {
          return vm.pokemon.results == null
              ? Center(child: CircularProgressIndicator())
              : PokemonList(
                  results: vm.pokemon.results.toList(),
                  onLoadMoreTapped: vm.onLoadMoreTapped,
                  onPokemonTapped: (result) =>
                      _navigateToPokemonDetailsPage(context, result),
                );
        },
      ),
    );
  }
}
