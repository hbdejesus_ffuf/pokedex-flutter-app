import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';

class LoadMoreCard extends StatelessWidget {
  final Function onTap;

  const LoadMoreCard({
    @required this.onTap,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: LoadingWidget(
        futureCallback: onTap,
        renderChild: (_, tap) => InkWell(
          onTap: tap,
          child: Icon(Icons.arrow_forward),
        ),
      ),
    );
  }
}
