import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/utilities/extensions.dart';

class PokemonListItem extends StatelessWidget {
  final Result result;
  final VoidCallback onPokemonTapped;

  const PokemonListItem({
    Key key,
    this.result,
    this.onPokemonTapped,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onPokemonTapped,
        child: Column(
          children: <Widget>[
            Hero(
              child: Image.asset(
                result.getPokemonAssetImage(),
                scale: 1.5,
              ),
              tag: result.getId(),
            ),
            Text(
              capitalizeFirstLetter(result.name),
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
    );
  }
}
