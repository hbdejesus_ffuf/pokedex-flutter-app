import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/features/home/widget/load_more_card.dart';
import 'package:pokemon_app/features/home/widget/pokemon_list_item.dart';

class PokemonList extends StatelessWidget {
  final List<Result> results;
  final Function onLoadMoreTapped;
  final Function(Result) onPokemonTapped;

  const PokemonList({
    Key key,
    this.results,
    this.onLoadMoreTapped,
    this.onPokemonTapped,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final children = [
      ...results.map(
        (result) => PokemonListItem(
          result: result,
          onPokemonTapped: () => onPokemonTapped(result),
        ),
      ),
      LoadMoreCard(onTap: onLoadMoreTapped)
    ];

    return GridView.count(
      children: children,
      crossAxisCount: 3,
    );
  }
}
