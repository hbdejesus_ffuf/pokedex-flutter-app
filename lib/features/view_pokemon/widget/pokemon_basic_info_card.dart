import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';

class PokemonBasicInformationCard extends StatelessWidget {
  final PokemonPokemon pokemonPokemon;

  const PokemonBasicInformationCard({Key key, this.pokemonPokemon})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'Type: ',
                  style: Theme.of(context).textTheme.display1,
                ),
                Row(
                  children: pokemonPokemon.types
                      .map((t) => Image.asset(
                            'images/${t.type.name}/${t.type.name}.png',
                            height: 25,
                          ))
                      .toList(),
                )
              ],
            ),
            SizedBox(height: 10),
            Text.rich(
                TextSpan(children: [
                  TextSpan(text: 'Height: '),
                  TextSpan(text: pokemonPokemon.height.toString())
                ]),
                style: Theme.of(context).textTheme.display1),
            SizedBox(height: 10),
            Text.rich(
              TextSpan(children: [
                TextSpan(text: 'Weight: '),
                TextSpan(text: pokemonPokemon.weight.toString())
              ]),
              style: Theme.of(context).textTheme.display1,
            ),
            SizedBox(height: 10),
            Text.rich(
              TextSpan(children: [
                TextSpan(text: 'Base Experience: '),
                TextSpan(text: pokemonPokemon.baseExperience.toString())
              ]),
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
    );
  }
}
