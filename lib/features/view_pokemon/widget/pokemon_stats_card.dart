import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_stats.dart';

class PokemonStatsCard extends StatelessWidget {
  final BuiltList<PokemonPokemonStats> stats;

  const PokemonStatsCard({Key key, this.stats}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: stats
              .map((stat) => Row(
                    children: <Widget>[
                      Text(stat.stat.name),
                      Spacer(),
                      Text(stat.baseStat.toString())
                    ],
                  ))
              .toList(),
        ),
      ),
    );
  }
}
