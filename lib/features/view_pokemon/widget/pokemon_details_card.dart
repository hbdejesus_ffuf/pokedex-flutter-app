import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';
import 'package:pokemon_app/features/view_pokemon/widget/pokemon_basic_info_card.dart';
import 'package:pokemon_app/features/view_pokemon/widget/pokemon_stats_card.dart';

class PokemonDetailsCard extends StatelessWidget {
  final PokemonPokemon pokemonPokemon;

  const PokemonDetailsCard({Key key, this.pokemonPokemon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return pokemonPokemon == null
        ? Center(child: CircularProgressIndicator())
        : _PokemonDetailsCardInfo(pokemonPokemon: pokemonPokemon);
  }
}

class _PokemonDetailsCardInfo extends StatelessWidget {
  final PokemonPokemon pokemonPokemon;

  const _PokemonDetailsCardInfo({Key key, this.pokemonPokemon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Basic Information',
            style: Theme.of(context).textTheme.headline,
          ),
          SizedBox(height: 10),
          PokemonBasicInformationCard(pokemonPokemon: pokemonPokemon),
          SizedBox(height: 30),
          Text(
            'Stats',
            style: Theme.of(context).textTheme.headline,
          ),
          PokemonStatsCard(
            stats: pokemonPokemon.stats,
          )
        ],
      ),
    );
  }
}
