import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/features/view_pokemon/widget/pokemon_details_card.dart';
import 'package:pokemon_app/state/actions.dart';
import 'package:pokemon_app/state/app_state.dart';
import 'package:pokemon_app/utilities/extensions.dart';

class PokemonDetailsArguments {
  final int id;

  PokemonDetailsArguments(this.id);
}

class _ViewPokemonPageVM extends BaseModel<AppState> {
  _ViewPokemonPageVM(this.id);

  _ViewPokemonPageVM.build({this.result, this.pokemonPokemon})
      : super(equals: [pokemonPokemon]);

  int id;
  Result result;
  PokemonPokemon pokemonPokemon;

  @override
  BaseModel fromStore() {
    return _ViewPokemonPageVM.build(
        // add a fallback whenever id's don't match
        result: state.pokemon.results.firstWhere(
          (r) => r.getId() == id,
          orElse: () => null,
        ),
        pokemonPokemon: state.pokemonPokemon);
  }
}

class PokemonDetailsPage extends StatefulWidget {
  static const String route = 'pokemon_details';

  final PokemonDetailsArguments arguments;

  const PokemonDetailsPage({
    Key key,
    this.arguments,
  }) : super(key: key);

  @override
  _PokemonDetailsPageState createState() => _PokemonDetailsPageState();
}

class _PokemonDetailsPageState extends State<PokemonDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewPokemonPageVM>(
      onInit: (store) => store
          .dispatchFuture(GetPokemonInformationAction(widget.arguments.id)),
      model: _ViewPokemonPageVM(widget.arguments.id),
      builder: (BuildContext context, _ViewPokemonPageVM vm) {
        return Scaffold(
          appBar: AppBar(
            title: Text(capitalizeFirstLetter(vm.result.name)),
          ),
          body: Container(
            constraints: BoxConstraints.expand(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Hero(
                    tag: widget.arguments.id,
                    child: Image.asset(
                      vm.result.getPokemonAssetImage(),
                      scale: .75,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                PokemonDetailsCard(pokemonPokemon: vm.pokemonPokemon)
              ],
            ),
          ),
        );
      },
    );
  }
}
