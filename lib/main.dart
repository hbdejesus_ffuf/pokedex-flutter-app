import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_app/state/app_state.dart';
import 'package:pokemon_app/pokemon_app.dart';

void main() async {

  final Store<AppState> store = await initializeStore();

  runApp(PokemonApp(store: store));
}

