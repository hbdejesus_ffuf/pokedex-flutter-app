import 'package:flutter/material.dart';
import 'package:pokemon_app/features/home/home_page.dart';
import 'package:pokemon_app/features/view_pokemon/pokemon_details_page.dart';

final navigatorKey = GlobalKey<NavigatorState>();

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomePage.route:
        return MaterialPageRoute(builder: (_) => HomePage());
      case PokemonDetailsPage.route:
        return MaterialPageRoute(
          builder: (_) => PokemonDetailsPage(
            arguments: settings.arguments,
          ),
        );
      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}
