import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_app/features/home/home_page.dart';
import 'package:pokemon_app/state/app_state.dart';
import 'package:pokemon_app/utilities/app_router.dart';

Future<Store<AppState>> initializeStore() async {
  NavigateAction.setNavigatorKey(navigatorKey);

  final persistor = Persistor<AppState>(
    storage: StandardEngine('pokemon_app'),
    serializer: AppStateSerializer(),
    version: 1,
    debug: true,
  );

  AppState initialState;
  try {
    initialState = await persistor.load();
  } on Exception catch (e) {
    print(e);
  }

  final store = Store<AppState>(
    initialState: initialState ?? AppState(),
    actionObservers: [Log.printer(formatter: Log.verySimpleFormatter)],
    stateObservers: [persistor.createPersistObserver()],
  );

  return store;
}

/// To display a loading animation make PokemonApp a [StatefulWidget] and call [initializeStore] in initState().
class PokemonApp extends StatelessWidget {
  const PokemonApp({this.store});
  final Store<AppState> store;

  @override
  Widget build(BuildContext context) {
    /// makes the store accessible to the app to get the state or dispatch actions
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'PokemonApp',
        theme: ThemeData(
          textTheme: TextTheme(
            display1: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),

        /// UserExceptions can be visualized with a Flushbar to User app-wide
        home: UserExceptionWidget<AppState>(
          child: WillPopScope(
            /// To intercept the Android Back button
            onWillPop: () async => !await navigatorKey.currentState.maybePop(),
            child: Navigator(
              key: navigatorKey,
              initialRoute: HomePage.route,
              onGenerateRoute: AppRouter.generateRoute,
            ),
          ),
          requestFlushbar: (String key, BuildContext bContext) => Flushbar(
            title: key,
            message: key,
          ),
        ),
      ),
    );
  }
}
