import 'package:dio/dio.dart';

class ApiClient {
  ApiClient() {
    dio.options.baseUrl = 'https://pokeapi.co/api/v2/';
  }

  final dio = Dio();
}
