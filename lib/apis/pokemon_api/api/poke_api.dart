import 'package:dio/dio.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';
import 'package:pokemon_app/apis/project_api/api_client.dart';
import 'package:pokemon_app/state/serializers.dart';

class PokeApi {
  final ApiClient apiClient;

  PokeApi([ApiClient apiClient]) : apiClient = apiClient ?? ApiClient();

  /*
    Get all Pokemon
  */

  Future<Pokemon> getAllPokemon({int offset: 0, int limit: 20}) async {
    String path = 'pokemon?offset=$offset&limit=$limit';
    Response response = await apiClient.dio.get(path);

    if (response.data != null) {
      return deserializeOf<Pokemon>(response.data);
    } else {
      return null;
    }
  }

  Future<PokemonPokemon> getPokemonById(int id) async {
    String path = 'pokemon/$id/';
    Response response = await apiClient.dio.get(path);

    if (response.data != null) {
      return deserializeOf<PokemonPokemon>(response.data);
    } else {
      return null;
    }
  }
}
