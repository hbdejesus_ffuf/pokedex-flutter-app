import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'basic_response.g.dart';
// STARTER: import - do not remove comment

abstract class BasicResponse
    implements Built<BasicResponse, BasicResponseBuilder> {
  /// Initializes a new instance of the [BasicResponse] class
  factory BasicResponse([updates(BasicResponseBuilder b)]) = _$BasicResponse;

  BasicResponse._();

  @nullable
  String get name;

  @nullable
  String get url;

  /// serializer
  static Serializer<BasicResponse> get serializer => _$basicResponseSerializer;

  /// get BasicResponse from Json
  static BasicResponse fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
