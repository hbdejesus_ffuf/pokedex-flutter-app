import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

import 'basic_response.dart';

part 'pokemon_pokemon_moves_version_group_details.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonMovesVersionGroupDetails
    implements
        Built<PokemonPokemonMovesVersionGroupDetails,
            PokemonPokemonMovesVersionGroupDetailsBuilder> {
  /// Initializes a new instance of the [PokemonPokemonMovesVersionGroupDetails] class
  factory PokemonPokemonMovesVersionGroupDetails(
          [updates(PokemonPokemonMovesVersionGroupDetailsBuilder b)]) =
      _$PokemonPokemonMovesVersionGroupDetails;

  PokemonPokemonMovesVersionGroupDetails._();

  @BuiltValueField(wireName: 'level_learned_at')
  @nullable
  int get levelLearnedAt;

  @BuiltValueField(wireName: 'move_learn_method')
  @nullable
  BasicResponse get moveLearnMethod;

  @BuiltValueField(wireName: 'version_group')
  @nullable
  BasicResponse get versionGroup;

  /// serializer
  static Serializer<PokemonPokemonMovesVersionGroupDetails> get serializer =>
      _$pokemonPokemonMovesVersionGroupDetailsSerializer;

  /// get PokemonPokemonMovesVersionGroupDetails from Json
  static PokemonPokemonMovesVersionGroupDetails fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
