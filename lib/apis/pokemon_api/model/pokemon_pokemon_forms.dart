import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/apis/pokemon_api/model/basic_response.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_forms.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonForms
    implements Built<PokemonPokemonForms, PokemonPokemonFormsBuilder> {
  /// Initializes a new instance of the [PokemonPokemonForms] class
  factory PokemonPokemonForms([updates(PokemonPokemonFormsBuilder b)]) =
      _$PokemonPokemonForms;

  PokemonPokemonForms._();

  @BuiltValueField()
  @nullable
  BuiltList<BasicResponse> get forms;

  /// serializer
  static Serializer<PokemonPokemonForms> get serializer =>
      _$pokemonPokemonFormsSerializer;

  /// get PokemonPokemonForms from Json
  static PokemonPokemonForms fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
