import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon.g.dart';
// STARTER: import - do not remove comment

abstract class Pokemon implements Built<Pokemon, PokemonBuilder> {
  /// Initializes a new instance of the [Pokemon] class
  factory Pokemon([updates(PokemonBuilder b)]) = _$Pokemon;

  Pokemon._();

  @nullable
  int get count;

  @nullable
  String get next;

  @nullable
  String get previous;

  @nullable
  BuiltList<Result> get results;

  /// serializer
  static Serializer<Pokemon> get serializer => _$pokemonSerializer;

  /// get Pokemon from Json
  static Pokemon fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
