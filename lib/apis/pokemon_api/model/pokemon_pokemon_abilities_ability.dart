import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_abilities_ability.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonAbilitiesAbility
    implements
        Built<PokemonPokemonAbilitiesAbility,
            PokemonPokemonAbilitiesAbilityBuilder> {
  /// Initializes a new instance of the [PokemonPokemonAbilitiesAbility] class
  factory PokemonPokemonAbilitiesAbility(
          [updates(PokemonPokemonAbilitiesAbilityBuilder b)]) =
      _$PokemonPokemonAbilitiesAbility;

  PokemonPokemonAbilitiesAbility._();

  @nullable
  String get name;

  @nullable
  String get url;

  /// serializer
  static Serializer<PokemonPokemonAbilitiesAbility> get serializer =>
      _$pokemonPokemonAbilitiesAbilitySerializer;

  /// get PokemonPokemonAbilitiesAbility from Json
  static PokemonPokemonAbilitiesAbility fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
