import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/apis/pokemon_api/model/basic_response.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_abilities.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonAbilities
    implements Built<PokemonPokemonAbilities, PokemonPokemonAbilitiesBuilder> {
  /// Initializes a new instance of the [PokemonPokemonAbilities] class
  factory PokemonPokemonAbilities([updates(PokemonPokemonAbilitiesBuilder b)]) =
      _$PokemonPokemonAbilities;

  PokemonPokemonAbilities._();

  @nullable
  int get slot;

  @nullable
  bool get isHidden;

  @BuiltValueField()
  @nullable
  BasicResponse get ability;

  /// serializer
  static Serializer<PokemonPokemonAbilities> get serializer =>
      _$pokemonPokemonAbilitiesSerializer;

  /// get PokemonPokemonAbilities from Json
  static PokemonPokemonAbilities fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
