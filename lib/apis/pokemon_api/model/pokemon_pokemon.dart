import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_abilities.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_forms.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_game_indices.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_moves.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_sprites.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_stats.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_types.dart';
import 'package:pokemon_app/state/serializers.dart';

import 'basic_response.dart';

part 'pokemon_pokemon.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemon
    implements Built<PokemonPokemon, PokemonPokemonBuilder> {
  /// Initializes a new instance of the [PokemonPokemon] class
  ///
  factory PokemonPokemon([updates(PokemonPokemonBuilder b)]) = _$PokemonPokemon;

  PokemonPokemon._();

  @nullable
  int get id;

  @BuiltValueField()
  @nullable
  BuiltList<PokemonPokemonAbilities> get abilities;

  @BuiltValueField(wireName: 'base_experience')
  @nullable
  int get baseExperience;

  @BuiltValueField()
  @nullable
  BuiltList<PokemonPokemonForms> get forms;

  @BuiltValueField(wireName: 'game_indices')
  @nullable
  BuiltList<PokemonPokemonGameIndeces> get gameIndices;

  @nullable
  int get height;

  ///Held Items

  @BuiltValueField(wireName: 'is_default')
  @nullable
  bool get isDefault;

  @BuiltValueField(wireName: 'location_area_encounters')
  @nullable
  String get locationAreaEncounter;

  @BuiltValueField()
  @nullable
  BuiltList<PokemonPokemonMoves> get moves;

  @nullable
  String get name;

  @nullable
  int get order;

  @BuiltValueField()
  @nullable
  BasicResponse get species;

  @BuiltValueField()
  @nullable
  PokemonPokemonSprites get sprites;

  @BuiltValueField()
  @nullable
  BuiltList<PokemonPokemonStats> get stats;

  @BuiltValueField()
  @nullable
  BuiltList<PokemonPokemonTypes> get types;

  @nullable
  int get weight;

  /// serializer
  static Serializer<PokemonPokemon> get serializer =>
      _$pokemonPokemonSerializer;

  /// get PokemonPokemon from Json
  static PokemonPokemon fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
