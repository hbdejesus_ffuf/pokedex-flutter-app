import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'result.g.dart';
// STARTER: import - do not remove comment

abstract class Result implements Built<Result, ResultBuilder> {
  /// Initializes a new instance of the [Result] class
  factory Result([updates(ResultBuilder b)]) = _$Result;

  Result._();

  String get name;

  String get url;

  String getPokemonAssetImage() {
    int id = getId();
    String assetUrl = 'images/$id/$id.png';
    return assetUrl;
  }

  int getId() {
    List<String> x = url.split('/');
    String id = x[x.length - 2];

    return int.tryParse(id);
  }

  /// serializer
  static Serializer<Result> get serializer => _$resultSerializer;

  /// get Result from Json
  static Result fromJson(dynamic json) => standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
