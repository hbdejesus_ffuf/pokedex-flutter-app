import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_game_indices_version.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_game_indices.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonGameIndeces
    implements
        Built<PokemonPokemonGameIndeces, PokemonPokemonGameIndecesBuilder> {
  /// Initializes a new instance of the [PokemonPokemonGameIndeces] class
  factory PokemonPokemonGameIndeces(
          [updates(PokemonPokemonGameIndecesBuilder b)]) =
      _$PokemonPokemonGameIndeces;

  PokemonPokemonGameIndeces._();

  @nullable
  int get gameIndex;

  PokemonPokemonGameIndecesVersion get version;

  /// serializer
  static Serializer<PokemonPokemonGameIndeces> get serializer =>
      _$pokemonPokemonGameIndecesSerializer;

  /// get PokemonPokemonGameIndeces from Json
  static PokemonPokemonGameIndeces fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
