import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

import 'basic_response.dart';

part 'pokemon_pokemon_types.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonTypes
    implements Built<PokemonPokemonTypes, PokemonPokemonTypesBuilder> {
  /// Initializes a new instance of the [PokemonPokemonTypes] class
  factory PokemonPokemonTypes([updates(PokemonPokemonTypesBuilder b)]) =
      _$PokemonPokemonTypes;

  PokemonPokemonTypes._();

  @nullable
  int get slot;

  @BuiltValueField()
  @nullable
  BasicResponse get type;

  /// serializer
  static Serializer<PokemonPokemonTypes> get serializer =>
      _$pokemonPokemonTypesSerializer;

  /// get PokemonPokemonTypes from Json
  static PokemonPokemonTypes fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
