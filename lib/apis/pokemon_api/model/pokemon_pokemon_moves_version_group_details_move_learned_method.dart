import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_moves_version_group_details_move_learned_method.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod
    implements
        Built<PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod,
            PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethodBuilder> {
  /// Initializes a new instance of the [PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod] class
  factory PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod(
      [updates(
          PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethodBuilder
              b)]) = _$PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod;

  PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod._();

  /// serializer
  static Serializer<PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod>
      get serializer =>
          _$pokemonPokemonMovesVersionGroupDetailsMoveLearnedMethodSerializer;

  /// get PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod from Json
  static PokemonPokemonMovesVersionGroupDetailsMoveLearnedMethod fromJson(
          dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
