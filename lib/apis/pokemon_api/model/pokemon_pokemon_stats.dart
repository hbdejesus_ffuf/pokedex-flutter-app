import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

import 'basic_response.dart';

part 'pokemon_pokemon_stats.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonStats
    implements Built<PokemonPokemonStats, PokemonPokemonStatsBuilder> {
  /// Initializes a new instance of the [PokemonPokemonStats] class
  factory PokemonPokemonStats([updates(PokemonPokemonStatsBuilder b)]) =
      _$PokemonPokemonStats;

  PokemonPokemonStats._();

  @BuiltValueField(wireName: 'base_stat')
  @nullable
  int get baseStat;

  @nullable
  int get effort;

  @nullable
  BasicResponse get stat;

  /// serializer
  static Serializer<PokemonPokemonStats> get serializer =>
      _$pokemonPokemonStatsSerializer;

  /// get PokemonPokemonStats from Json
  static PokemonPokemonStats fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
