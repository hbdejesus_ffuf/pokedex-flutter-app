import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_moves_move.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonMovesMove
    implements Built<PokemonPokemonMovesMove, PokemonPokemonMovesMoveBuilder> {
  /// Initializes a new instance of the [PokemonPokemonMovesMove] class
  factory PokemonPokemonMovesMove([updates(PokemonPokemonMovesMoveBuilder b)]) =
      _$PokemonPokemonMovesMove;

  PokemonPokemonMovesMove._();

  @nullable
  String get name;

  @nullable
  String get url;

  /// serializer
  static Serializer<PokemonPokemonMovesMove> get serializer =>
      _$pokemonPokemonMovesMoveSerializer;

  /// get PokemonPokemonMovesMove from Json
  static PokemonPokemonMovesMove fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
