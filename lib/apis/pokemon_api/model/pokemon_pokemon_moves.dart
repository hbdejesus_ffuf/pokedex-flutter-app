import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_moves_version_group_details.dart';
import 'package:pokemon_app/state/serializers.dart';

import 'basic_response.dart';

part 'pokemon_pokemon_moves.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonMoves
    implements Built<PokemonPokemonMoves, PokemonPokemonMovesBuilder> {
  /// Initializes a new instance of the [PokemonPokemonMoves] class
  factory PokemonPokemonMoves([updates(PokemonPokemonMovesBuilder b)]) =
      _$PokemonPokemonMoves;

  PokemonPokemonMoves._();

  @BuiltValueField()
  @nullable
  BasicResponse get move;

  @BuiltValueField(wireName: 'version_group_details')
  @nullable
  BuiltList<PokemonPokemonMovesVersionGroupDetails> get versionGroupDetails;

  /// serializer
  static Serializer<PokemonPokemonMoves> get serializer =>
      _$pokemonPokemonMovesSerializer;

  /// get PokemonPokemonMoves from Json
  static PokemonPokemonMoves fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
