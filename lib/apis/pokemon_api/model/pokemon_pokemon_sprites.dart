import 'dart:ffi';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_sprites.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonSprites
    implements Built<PokemonPokemonSprites, PokemonPokemonSpritesBuilder> {
  /// Initializes a new instance of the [PokemonPokemonSprites] class
  factory PokemonPokemonSprites([updates(PokemonPokemonSpritesBuilder b)]) =
      _$PokemonPokemonSprites;

  PokemonPokemonSprites._();

  @nullable
  String get backDefault;

  @nullable
  String get backFemail;

  @nullable
  String get backShiny;

  @nullable
  String get backShinyFemale;

  @nullable
  String get frontDefault;

  @nullable
  String get frontFemale;

  @nullable
  String get frontShiny;

  @nullable
  String get frontShinyFemale;

  /// serializer
  static Serializer<PokemonPokemonSprites> get serializer =>
      _$pokemonPokemonSpritesSerializer;

  /// get PokemonPokemonSprites from Json
  static PokemonPokemonSprites fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
