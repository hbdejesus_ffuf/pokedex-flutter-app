import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'pokemon_pokemon_game_indices_version.g.dart';
// STARTER: import - do not remove comment

abstract class PokemonPokemonGameIndecesVersion
    implements
        Built<PokemonPokemonGameIndecesVersion,
            PokemonPokemonGameIndecesVersionBuilder> {
  /// Initializes a new instance of the [PokemonPokemonGameIndecesVersion] class
  factory PokemonPokemonGameIndecesVersion(
          [updates(PokemonPokemonGameIndecesVersionBuilder b)]) =
      _$PokemonPokemonGameIndecesVersion;

  PokemonPokemonGameIndecesVersion._();

  @nullable
  String get name;

  @nullable
  String get url;

  /// serializer
  static Serializer<PokemonPokemonGameIndecesVersion> get serializer =>
      _$pokemonPokemonGameIndecesVersionSerializer;

  /// get PokemonPokemonGameIndecesVersion from Json
  static PokemonPokemonGameIndecesVersion fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  ///convert to Json
  dynamic toJson() => standardSerializers.serialize(this);
}
