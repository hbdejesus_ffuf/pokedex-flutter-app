import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokemon_app/apis/pokemon_api/model/basic_response.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_abilities.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_forms.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_game_indices.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_game_indices_version.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_moves.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_moves_version_group_details.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_sprites.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_stats.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon_types.dart';
import 'package:pokemon_app/apis/pokemon_api/model/result.dart';
import 'package:pokemon_app/state/app_state.dart';

/// import needed models and add them to the @SerializersFor annotation list

part 'serializers.g.dart';

@SerializersFor([AppState, PokemonPokemon])
final Serializers serializers = _$serializers;

final Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();

/// Helper function to deserialize a specific Type
T deserializeOf<T>(dynamic value) => standardSerializers.deserializeWith<T>(
    standardSerializers.serializerForType(T), value);

/// Helper function to deserialize a BuiltList of a specific Type
BuiltList<T> deserializeListOf<T>(dynamic value) =>
    BuiltList.from(value.map(deserializeOf<T>(value)).toList(growable: false));
