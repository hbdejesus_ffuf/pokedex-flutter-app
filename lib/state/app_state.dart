import 'dart:convert';

import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon.dart';
import 'package:pokemon_app/apis/pokemon_api/model/pokemon_pokemon.dart';
import 'package:pokemon_app/state/serializers.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  factory AppState() => _$AppState._(
        pokemon: Pokemon(),
        pokemonPokemon: null,
      );

  AppState._();

  @nullable
  Pokemon get pokemon;

  @nullable
  PokemonPokemon get pokemonPokemon;

  static Serializer<AppState> get serializer => _$appStateSerializer;

  static AppState fromJson(dynamic json) =>
      standardSerializers.deserialize(json);

  String toJson() => jsonEncode(standardSerializers.serialize(this));
}

class AppStateSerializer extends StateSerializer<AppState> {
  @override
  String encode(AppState state) => state.toJson();

  @override
  AppState decode(dynamic data) =>
      data != null ? AppState.fromJson(data) : AppState();
}
