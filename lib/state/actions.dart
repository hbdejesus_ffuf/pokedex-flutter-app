import 'dart:async';

import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokemon_app/apis/pokemon_api/api/poke_api.dart';
import 'package:pokemon_app/state/app_state.dart';

/// Create ReduxAction's to dispatch, that will rebuild the AppState

class GetAllPokemonAction extends ReduxAction<AppState> {
  @override
  Future<AppState> reduce() async {
    final pkmn = await PokeApi()
        .getAllPokemon(offset: state.pokemon.results?.length ?? 0);
    return state.rebuild((b) => b
      ..pokemon.next = pkmn.next
      ..pokemon.results.addAll(pkmn.results));
  }
}

class GetPokemonInformationAction extends ReduxAction<AppState> {
  final int id;

  GetPokemonInformationAction(this.id);

  @override
  FutureOr<AppState> reduce() async {
    final pokemonPokemon = await PokeApi().getPokemonById(id);

    return state.rebuild((b) => b.pokemonPokemon.replace(pokemonPokemon));
  }
}
